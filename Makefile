all: bin/texcube bin/texcubeIL bin/VBO

bin/texcube: src/texcube.c
	gcc -o bin/texcube src/texcube.c -lIL -lglut -lGLU -lGL

bin/texcubeIL: src/texcube.c
	gcc -o bin/texcubeIL src/texcube.c -lIL -lglut -lGLU -lGL -DIL_VERSION

bin/VBO: src/VBO.c include/point.h include/color.h
	gcc -o bin/VBO src/VBO.c -lglut -lGLU -lGL -Iinclude

clean:
	rm bin/texcube bin/texcubeIL bin/VBO
